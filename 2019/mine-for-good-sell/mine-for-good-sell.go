package main

import (
	"fmt"
)

func main() {
    fmt.Println("hello")

	desktopHashesPerSecond := 47.0
	laptopHashesPerSecond := 20.0
	mobileHashesPerSecond := 7.0
	
	miningPoolRewardPerMillionHashesXmr := 0.000058678216
	xmrPriceUsd := 105.78
	usdPerHash := (miningPoolRewardPerMillionHashesXmr / 1000000) * xmrPriceUsd

	fmt.Println("usdPerHash: ", usdPerHash)

	desktopUsdPerSecond := desktopHashesPerSecond * usdPerHash
	laptopUsdPerSecond := laptopHashesPerSecond * usdPerHash
	mobileUsdPerSecond := mobileHashesPerSecond * usdPerHash

	fmt.Println("desktop usd per second: ", desktopUsdPerSecond)
	fmt.Println("laptop usd per second: ", laptopUsdPerSecond)
	fmt.Println("mobile usd per second: ", mobileUsdPerSecond)

	secondsPerMinute := 60.0
	secondsPerHour := secondsPerMinute * 60.0
	secondsPerDay := secondsPerHour * 24.0
	secondsPerYear := secondsPerDay * 365

	fmt.Println("seconds per year: ", secondsPerYear)

	desktopUsdPerYear := desktopUsdPerSecond * secondsPerYear
	laptopUsdPerYear := laptopUsdPerSecond * secondsPerYear
	mobileUsdPerYear := mobileUsdPerSecond * secondsPerYear

	fmt.Println("desktop usd per year: ", desktopUsdPerYear)
	fmt.Println("laptop usd per year: ", laptopUsdPerYear)
	fmt.Println("mobile usd per year: ", mobileUsdPerYear)

	secondsPerSixHours := secondsPerHour * 6.0
	secondsPerYearAtSixHoursPerDay := secondsPerSixHours * 365

	fmt.Println("seconds per year at six hours per day: ", secondsPerYearAtSixHoursPerDay)

	desktopUsdPerYearAtSixHoursPerDay := desktopUsdPerSecond * secondsPerYearAtSixHoursPerDay
	laptopUsdPerYearAtSixHoursPerDay := laptopUsdPerSecond * secondsPerYearAtSixHoursPerDay
	mobileUsdPerYearAtSixHoursPerDay := mobileUsdPerSecond * secondsPerYearAtSixHoursPerDay

	fmt.Println("desktop usd per year at six hours per day: ", desktopUsdPerYearAtSixHoursPerDay)
	fmt.Println("laptop usd per year at six hours per day: ", laptopUsdPerYearAtSixHoursPerDay)
	fmt.Println("mobile usd per year at six hours per day: ", mobileUsdPerYearAtSixHoursPerDay)

	daysInWorkWeek := 5.0
	workWeeksInYear := 48.0
	daysInWorkYear := daysInWorkWeek * workWeeksInYear
	secondsPerWorkYearAtSixHoursPerDay := daysInWorkYear * secondsPerSixHours

	fmt.Println("seconds per work year at six hours per day: ", secondsPerWorkYearAtSixHoursPerDay)

	desktopUsdPerWorkYearAtSixHoursPerDay := desktopUsdPerSecond * secondsPerWorkYearAtSixHoursPerDay
	laptopUsdPerWorkYearAtSixHoursPerDay := laptopUsdPerSecond * secondsPerWorkYearAtSixHoursPerDay
	mobileUsdPerWorkYearAtSixHoursPerDay := mobileUsdPerSecond * secondsPerWorkYearAtSixHoursPerDay

	fmt.Println("desktop usd per work year at six hours per day: ", desktopUsdPerWorkYearAtSixHoursPerDay)
	fmt.Println("laptop usd per work year at six hours per day: ", laptopUsdPerWorkYearAtSixHoursPerDay)
	fmt.Println("mobile usd per work year at six hours per day: ", mobileUsdPerWorkYearAtSixHoursPerDay)

}