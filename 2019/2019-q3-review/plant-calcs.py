
# carbon savings

# these numbers pulled from http://static.ewg.org/reports/2011/meateaters/pdf/methodology_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf
lamb_carbon_per_kg = 39.2
beef_carbon_per_kg = 27.0
pork_carbon_per_kg = 12.1
fish_carbon_per_kg = 11.9 # we use the salmon metric here cause it's the worst
chicken_carbon_per_kg = 6.9
vegetable_carbon_per_kg = 2.9 # we use the potatoes metric cause it's the worst non-meat


# resource savings

# these numbers pulled from https://www.wri.org/resources/charts-graphs/animal-based-foods-are-more-resource-intensive-plant-based-foods
# using water resource metric here as it's the highest and most conservative
# wrt beef being worse than other meats and therefore should give us a 
# more accurate lower bound for resource savings

# lamb wasn't listed in this resource so we're just going to assume
# it's just as bad as beef which should be conservative wrt its 
# badness given how much worse it is in the carbon figures
lamb_resource_per_kg = 110
beef_resource_per_kg = 110
pork_resource_per_kg = 50
fish_resource_per_kg = 40
chicken_resource_per_kg = 30
vegetable_resource_per_kg = 25 # we use the roots & tubers metric cause it's the worst for non-meat

# Here we assume that I ate meat every meal, 3 meals a day
# I ate a lot of chicken so we'll say that I had chicken twice a day,
# pork or beef the other meal, and fish once a week. Due to the large
# amount of chicken, this should actually be under estimating the amount 
# of resources I used (as chicken is near the bottom) and thus
# should give us a pretty good upper bound for usage
def calculate_original_usage_per_week(
    lamb_usage: float,
    beef_usage: float,
    pork_usage: float,
    fish_usage: float,
    chicken_usage: float,
    vegetable_usage: float
) -> float:
    return (
        2 * (2 * chicken_usage + beef_usage)
        + 2 * (2 * chicken_usage + lamb_usage)
        + 2 * (2 * chicken_usage + pork_usage)
        + (2 * chicken_usage + fish_usage)
    )

def calculate_new_usage_per_week(
    lamb_usage: float,
    beef_usage: float,
    pork_usage: float,
    fish_usage: float,
    chicken_usage: float,
    vegetable_usage: float
) -> float:
    return (
        # meatless monday
        3 * vegetable_usage
        # beefless / lambless weekdays
        + 3 * (2 * chicken_usage + pork_usage)
        # fish-only fridays
        + 3 * fish_usage
        # anything sat / sun
        + (2 * chicken_usage + beef_usage)
        + (2 * chicken_usage + lamb_usage)
    )

def calculate_poulcetarian_usage_per_week(
    lamb_usage: float,
    beef_usage: float,
    pork_usage: float,
    fish_usage: float,
    chicken_usage: float,
    vegetable_usage: float
) -> float:
    return (
        # meatless monday
        3 * vegetable_usage
        # beefless / lambless weekdays
        + 4 * (2 * chicken_usage + fish_usage)
        # anything sat / sun
        + (2 * chicken_usage + beef_usage)
        + (2 * chicken_usage + lamb_usage)
    )

original_carbon_usage = calculate_original_usage_per_week(
    lamb_carbon_per_kg,
    beef_carbon_per_kg,
    pork_carbon_per_kg,
    fish_carbon_per_kg,
    chicken_carbon_per_kg,
    vegetable_carbon_per_kg
)
new_carbon_usage = calculate_new_usage_per_week(
    lamb_carbon_per_kg,
    beef_carbon_per_kg,
    pork_carbon_per_kg,
    fish_carbon_per_kg,
    chicken_carbon_per_kg,
    vegetable_carbon_per_kg
)
poulcetarian_carbon_usage =  calculate_poulcetarian_usage_per_week(
    lamb_carbon_per_kg,
    beef_carbon_per_kg,
    pork_carbon_per_kg,
    fish_carbon_per_kg,
    chicken_carbon_per_kg,
    vegetable_carbon_per_kg
)
print(f"new / old carbon percentage = {new_carbon_usage / original_carbon_usage}")
print(f"poulcetarian / old carbon percentage = {poulcetarian_carbon_usage / original_carbon_usage}")
print(f"poulcetarian / new carbon percentage = {poulcetarian_carbon_usage / new_carbon_usage}")


original_resource_usage = calculate_original_usage_per_week(
    lamb_resource_per_kg,
    beef_resource_per_kg,
    pork_resource_per_kg,
    fish_resource_per_kg,
    chicken_resource_per_kg,
    vegetable_resource_per_kg
)
new_resource_usage = calculate_new_usage_per_week(
    lamb_resource_per_kg,
    beef_resource_per_kg,
    pork_resource_per_kg,
    fish_resource_per_kg,
    chicken_resource_per_kg,
    vegetable_resource_per_kg
)
poulcetarian_resource_usage = calculate_poulcetarian_usage_per_week(
    lamb_resource_per_kg,
    beef_resource_per_kg,
    pork_resource_per_kg,
    fish_resource_per_kg,
    chicken_resource_per_kg,
    vegetable_resource_per_kg
)
print(f"new / old resource percentage = {new_resource_usage / original_resource_usage}")
print(f"poulcetarian / old resource percentage = {poulcetarian_resource_usage / original_resource_usage}")
print(f"poulcetarian / new resource percentage = {poulcetarian_resource_usage / new_resource_usage}")
