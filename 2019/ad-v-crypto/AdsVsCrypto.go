package main

import "fmt"

func main() {
	fmt.Println("Hello, world.")

	adRevenueUsdPerThousandImpressions := 2.50
	adRevenueUsdPerImpression := adRevenueUsdPerThousandImpressions / 1000
	fmt.Println("adRevenueUsdPerImpression: ", adRevenueUsdPerImpression)

	averageSessionDurationSeconds := 137.0

	cryptoTokenPriceUsd := 97.87

	hashesPerSecondDesktop := 50.0
	hashesPerSecondMobile := 6.0

	xmrRewardPerMillionHashes := 0.000064527455
	usdRewardPerMillionHashes := xmrRewardPerMillionHashes * cryptoTokenPriceUsd
	usdRewardPerHash := usdRewardPerMillionHashes / 1000000

	fmt.Println("usdRewardPerHash: ", usdRewardPerHash)

	valuePerVisitorCryptoMiningUsdOnDesktop := hashesPerSecondDesktop * usdRewardPerHash * averageSessionDurationSeconds
	valuePerVisitorCryptoMiningUsdOnMobile := hashesPerSecondMobile * usdRewardPerHash * averageSessionDurationSeconds
	fmt.Println("valuePerVisitorCryptoMiningUsdOnDesktop: ", valuePerVisitorCryptoMiningUsdOnDesktop)
	fmt.Println("valuePerVisitorCryptoMiningUsdOnMobile: ", valuePerVisitorCryptoMiningUsdOnMobile)

	fmt.Println("On average, ads are x better than crypto: ", adRevenueUsdPerImpression / valuePerVisitorCryptoMiningUsdOnDesktop)
	fmt.Println("ads are x better than crypto on phones: ", adRevenueUsdPerImpression / valuePerVisitorCryptoMiningUsdOnMobile)

	longFormContentSessionDurationSeconds := 600.0
	valuePerVisitorOfLongFormCryptoMiningUsdOnDesktop := hashesPerSecondDesktop * usdRewardPerHash * longFormContentSessionDurationSeconds
	fmt.Println("valuePerVisitorOfLongFormCryptoMiningUsdOnDesktop: ", valuePerVisitorOfLongFormCryptoMiningUsdOnDesktop)
	fmt.Println("ads are x better than longForm mining: ", adRevenueUsdPerImpression / valuePerVisitorOfLongFormCryptoMiningUsdOnDesktop)

	valuePerVisitorOfLongFormCryptoMiningUsdOnMobile := hashesPerSecondMobile * usdRewardPerHash * longFormContentSessionDurationSeconds
	fmt.Println("ads are x better than longform mining mobile: ", adRevenueUsdPerImpression / valuePerVisitorOfLongFormCryptoMiningUsdOnMobile)
}