package main
import (
	"fmt"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	fmt.Println("travel calculator starting...")

	amsterdamDestination := Destination { Name: "Amsterdam, Netherlands" }
	arlingtonDestination := Destination { Name: "Arlington, Virginia" }
	atlantaDestination := Destination { Name: "Atlanta, Georgia" }
	berlinDestination := Destination { Name: "Berlin, Germany" }
	bostonDestination := Destination { Name: "Boston, Massachussets" }
	chicagoDestination := Destination { Name: "Chicago, Illinois" }
	detroitDestination := Destination { Name: "Detroit, Michigan" }
	hiloDestination := Destination { Name: "Hilo, Hawaii" }
	indianapolisDestination := Destination { Name: "Indianapolis, Indiana" }
	konaDestination := Destination { Name: "Kona, Hawaii" }
	losAngelesDestination := Destination { Name: "Los Angeles, California" }
	nashvilleDestination := Destination { Name: "Nashville, Tennessee" }
	newYorkCityDestination := Destination { Name: "New York City, New York" }
	philadelphiaDestination := Destination { Name: "Philadelphia, Pennsylvania" }
	salzburgDestination := Destination { Name: "Salzburg, Austria" }
	sanFranciscoDestination := Destination { Name: "San Francisco, California" }
	seattleDestination := Destination { Name: "Seattle, Washington" }
	viennaDestination := Destination { Name: "Vienna, Austria" }
	yosemiteDestination := Destination { Name: "Yosemite, California" }
	
	var allAdventures []Adventure

	newYorkHousingInvestigation := Adventure {
		StartDestination: arlingtonDestination,
		Trips: []Trip {
			Trip {
				TravelType: bus,
				Destination: newYorkCityDestination,
				DistanceMiles: 234 },
			Trip {
				TravelType: bus,
				Destination: arlingtonDestination,
				DistanceMiles: 234 } } }
	allAdventures = append(allAdventures, newYorkHousingInvestigation)
	
	newYorkHousingInvestigation2 := Adventure {
		StartDestination: arlingtonDestination,
		Trips: []Trip {
			Trip {
				TravelType: train,
				Destination: newYorkCityDestination,
				DistanceMiles: 226 },
			Trip {
				TravelType: train,
				Destination: arlingtonDestination,
				DistanceMiles: 226 } } }
	allAdventures = append(allAdventures, newYorkHousingInvestigation2)
	

	newYorkMove := Adventure {
		StartDestination: arlingtonDestination,
		Trips: []Trip {
			Trip {
				TravelType: car,
				Destination: philadelphiaDestination,
				DistanceMiles: 146 },
			Trip {
				TravelType: car,
				Destination: newYorkCityDestination,
				DistanceMiles: 95 } } }
	allAdventures = append(allAdventures, newYorkMove)

	nashvilleWedding := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: nashvilleDestination,
				DistanceMiles: 760 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 760 } } }
	allAdventures = append(allAdventures, nashvilleWedding)

	yosemiteHalfMarathon := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: sanFranciscoDestination,
				DistanceMiles: 2569 },
			Trip {
				TravelType: car,
				Destination: yosemiteDestination,
				DistanceMiles: 167 },
			Trip {
				TravelType: car,
				Destination: sanFranciscoDestination,
				DistanceMiles: 167 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 2569 } } }
	allAdventures = append(allAdventures, yosemiteHalfMarathon)

	dcPride := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: train,
				Destination: arlingtonDestination,
				DistanceMiles: 226 },
			Trip {
				TravelType: train,
				Destination: newYorkCityDestination,
				DistanceMiles: 226 } } }
	allAdventures = append(allAdventures, dcPride)

	dcHackathon := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: train,
				Destination: arlingtonDestination,
				DistanceMiles: 226 },
			Trip {
				TravelType: train,
				Destination: newYorkCityDestination,
				DistanceMiles: 226 } } }
	allAdventures = append(allAdventures, dcHackathon)

	visitFamAtlanta := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: indianapolisDestination,
				DistanceMiles: 644 },
			Trip {
				TravelType: airPlane,
				Destination: atlantaDestination,
				DistanceMiles: 428 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 747 } } }
	allAdventures = append(allAdventures, visitFamAtlanta)

	weekendInChicago := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: chicagoDestination,
				DistanceMiles: 712 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 712 } } }
	allAdventures = append(allAdventures, weekendInChicago)

	hawaiiTrip := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: atlantaDestination,
				DistanceMiles: 747 },
			Trip {
				TravelType: airPlane,
				Destination: losAngelesDestination,
				DistanceMiles: 1935 },
			Trip {
				TravelType: airPlane,
				Destination: konaDestination,
				DistanceMiles: 2516 },
			Trip { 
				TravelType: car,
				Destination: hiloDestination,
				DistanceMiles: 60 },
			Trip {
				TravelType: car,
				Destination: konaDestination,
				DistanceMiles: 60 },
			Trip {
				TravelType: airPlane,
				Destination: seattleDestination,
				DistanceMiles: 2700 },
			Trip {
				TravelType: airPlane,
				Destination: detroitDestination,
				DistanceMiles: 1935 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 481 } } }
	allAdventures = append(allAdventures, hawaiiTrip)

	atlantaForCareerFair := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: atlantaDestination,
				DistanceMiles: 747 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 747 } } }
	allAdventures = append(allAdventures, atlantaForCareerFair)

	dcForTeamSplit := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: train,
				Destination: arlingtonDestination,
				DistanceMiles: 204 },
			Trip {
				TravelType: train,
				Destination: newYorkCityDestination,
				DistanceMiles: 204 } } }
	allAdventures = append(allAdventures, dcForTeamSplit)

	sfForQCon := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: sanFranciscoDestination,
				DistanceMiles: 2569 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 2569 } } }
	allAdventures = append(allAdventures, sfForQCon)

	atlantaForsellDinner := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: atlantaDestination,
				DistanceMiles: 747 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 747 } } }
	allAdventures = append(allAdventures, atlantaForsellDinner)

	thanksGivingWeekend := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: train,
				Destination: bostonDestination,
				DistanceMiles: 190 },
			Trip {
				TravelType: train,
				Destination: newYorkCityDestination,
				DistanceMiles: 190 } } }
	allAdventures = append(allAdventures, thanksGivingWeekend)

	europeTrip := Adventure {
		StartDestination: newYorkCityDestination,
		Trips: []Trip {
			Trip {
				TravelType: airPlane,
				Destination: amsterdamDestination,
				DistanceMiles: 3648 },
			Trip {
				TravelType: train,
				Destination: berlinDestination,
				DistanceMiles: 359 },
			Trip {
				TravelType: train,
				Destination: salzburgDestination,
				DistanceMiles: 326 },
			Trip {
				TravelType: train,
				Destination: viennaDestination,
				DistanceMiles: 156 },
			Trip {
				TravelType: airPlane,
				Destination: amsterdamDestination,
				DistanceMiles: 582 },
			Trip {
				TravelType: airPlane,
				Destination: arlingtonDestination,
				DistanceMiles: 3857 },
			Trip {
				TravelType: airPlane,
				Destination: newYorkCityDestination,
				DistanceMiles: 204 } } }
	allAdventures = append(allAdventures, europeTrip)

	// write out json

	outBytes, err := json.Marshal(allAdventures)
	if err != nil {
		fmt.Printf("an error occurred: %#v", err)
	} 
	// else {
	// 	outString := string(outBytes)
	// 	fmt.Printf("outString: %#v", outString)
	// }

	err = ioutil.WriteFile("./resources/2018-raw-travel-data.json", outBytes, 0644)
	if err != nil {
		panic(err)
	}

	// calculations

	// all 
	fmt.Println()
	totalDistanceTraveled := 0
	for _, Adventure := range allAdventures {
		totalDistanceTraveled = totalDistanceTraveled + calculateTotalAdventureDistance(Adventure)
	}
	fmt.Printf("total distance traveled: %#v miles", totalDistanceTraveled)

	fmt.Println()
	circumferenceOfEarthMiles := 24901.0
	timesHamTraveledAroundEarth := float64(totalDistanceTraveled) / circumferenceOfEarthMiles
	fmt.Printf("times ham traveled around the earth: %#v", timesHamTraveledAroundEarth)

	totalCitiesTraveledToCount := 0
	for _, adventure := range allAdventures {
		totalCitiesTraveledToCount = totalCitiesTraveledToCount + calculateNumberOfCitiesVisitedInAdventure(adventure)
	}
	fmt.Println()
	fmt.Printf("total cities traveled to: %#v", totalCitiesTraveledToCount)

	// miles by travelType

	fmt.Println()
	totalMilesMap := make(map[TravelType]int)
	for _, Adventure := range allAdventures {
		milesMap := calculateTotalAdventureDistanceByModeOfTransportation(Adventure)
		for travelType, distance := range milesMap {
			if totalMiles, ok := totalMilesMap[travelType]; ok {
				totalMilesMap[travelType] = totalMiles + distance
			} else {
				totalMilesMap[travelType] = distance
			}
		}
	}
	for travelType, distance := range totalMilesMap {
		fmt.Println()
		fmt.Printf("%#v: %#v", travelType, distance)
	}

	travelTypeOutBytes, travelTypeErr := json.Marshal(totalMilesMap)
	if travelTypeErr != nil {
		panic(travelTypeErr)
	}

	err = ioutil.WriteFile("./resources/2018-travel-by-traveltype.json", travelTypeOutBytes, 0644)
	if err != nil {
		panic(err)
	}

	// write all trips to file for parsing by geolog stuff
	writeTripDestinationsInAdventureToCsvFilePath(
		allAdventures,
		"./resources/2018-travel-locations-for-geolog.csv")
}

func calculateTotalAdventureDistance(Adventure Adventure) int {
	totalMiles := 0
	for _, Trip := range Adventure.Trips {
		totalMiles = totalMiles + Trip.DistanceMiles
	}
	return totalMiles
}

func calculateTravelFrequencyByType() {

}

func calculateTotalAdventureDistanceByModeOfTransportation(Adventure Adventure) map[TravelType]int {
	distanceByTravelTypeMap := make(map[TravelType]int)
	for _, Trip := range Adventure.Trips {
		if distanceMiles, ok := distanceByTravelTypeMap[Trip.TravelType]; ok {
			distanceByTravelTypeMap[Trip.TravelType] = distanceMiles + Trip.DistanceMiles
		} else {
			distanceByTravelTypeMap[Trip.TravelType] = Trip.DistanceMiles;
		}
	}
	return distanceByTravelTypeMap
}

func calculateNumberOfCitiesVisitedInAdventure(adventure Adventure) int {
	visitedCitiesCount := 0
	visitedCitiesMap := make(map[string]int)
	for _, trip := range adventure.Trips {
		if _, isPresent := visitedCitiesMap[trip.Destination.Name]; !isPresent {
			visitedCitiesMap[trip.Destination.Name] = 1
			visitedCitiesCount = visitedCitiesCount + 1
		}
	}

	return visitedCitiesCount
}

func writeTripDestinationsInAdventureToCsvFilePath(
	adventures []Adventure,
	outputFilePath string ) {

	file, err := os.Create(outputFilePath)
	checkError("Cannot create out file", err)
	defer file.Close()

	csvWriter := csv.NewWriter(file)
	defer csvWriter.Flush()

	headers := []string{ "location", "date" }
	headerLineOutputErr := csvWriter.Write(headers)
	checkError("Cannot write header", headerLineOutputErr)

	for _, adventure := range adventures {
		for _, trip := range adventure.Trips {
			tripCsvOutputSlice := make([]string, 0)
			tripCsvOutputSlice = append(tripCsvOutputSlice, trip.Destination.Name)
			tripCsvOutputSlice = append(tripCsvOutputSlice, "2018")
			err := csvWriter.Write(tripCsvOutputSlice)
			checkError("Cannot write output to file", err)
		}
	}
}

type Adventure struct {
	// start date
	// end date
	StartDestination Destination
	Trips []Trip
}

type Trip struct {
	TravelType TravelType
	DistanceMiles int
	Destination Destination
}

type Destination struct {
	Name string
}

type TravelType string

const (
	airPlane TravelType = "airPlane"
	bus TravelType = "bus"
	car TravelType = "car"
	train TravelType = "train"
)

func checkError(message string, err error) {
    if err != nil {
        log.Fatal(message, err)
    }
}