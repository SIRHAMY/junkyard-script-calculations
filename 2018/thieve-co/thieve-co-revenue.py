affiliateCommissionPerItem = 0.02
numberOfCommissionsToBreakEven = 1 / affiliateCommissionPerItem
print("commissions needed to break even = ", numberOfCommissionsToBreakEven)

# need to solve for number of views we need to get to get to 50 buys
# totalBuys = buyRate * totalViews
def getTotalViewsFromNumberOfBuysAndRateOfBuys(totalBuys, buyRate):
    return totalBuys / buyRate

clickThroughRate = 0.02
buyRate = 0.125

buyRateForTotalViews = clickThroughRate * buyRate

totalViewsForBreakEvenBuyNumber = getTotalViewsFromNumberOfBuysAndRateOfBuys(numberOfCommissionsToBreakEven, buyRateForTotalViews)
print("total views needed for proposed buy rate =", totalViewsForBreakEvenBuyNumber)

improvedClickThroughRate = 0.04
print("total views needed with improved CTR =", getTotalViewsFromNumberOfBuysAndRateOfBuys(numberOfCommissionsToBreakEven, improvedClickThroughRate * buyRate))

numberOfItemsGivenAway = 3
numberOfcommisionsToBreakEvenWithCrew = numberOfItemsGivenAway / affiliateCommissionPerItem
print("total views needed with crew rewards =", getTotalViewsFromNumberOfBuysAndRateOfBuys(numberOfcommisionsToBreakEvenWithCrew, improvedClickThroughRate * buyRate))