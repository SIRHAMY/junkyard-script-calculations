# HG: we don't have real numbers, so we estimate based on the frontpage that says
# * 140+ speakers
# * 11:1 attendee:speaker ratio
# source: https://qconsf.com/
def getTotalAttendees():
    totalSpeakers = 140
    attendeesPerSpeaker = 11

    return totalSpeakers * attendeesPerSpeaker

# def getAverageCostOfAttendeeBy

# **** BEGIN MAIN ****

print("I am running")

# It's a pay scale, so will need to bucket (maybe as bell curve? doesn't)
# really hold cause this isn't average but time series of sorts
totalAttendees = getTotalAttendees()
print("totalAttendees: ", totalAttendees)
ratioOfAttendeesWhoDidWorkshops = 0.3
numberOfRegularAttendees = (1 - ratioOfAttendeesWhoDidWorkshops) * totalAttendees # check
print("regularAttendees: ", numberOfRegularAttendees)
costOfRegularAttendance = 2095 # todo: time series to take avg of all buckets
numberOfDeluxeAttendees = ratioOfAttendeesWhoDidWorkshops * totalAttendees # check
print("deluxeAttendees: ", numberOfDeluxeAttendees)
costOfDeluxeAttendance = 3600 # todo: time series w buckets

# base sponsor info from: https://qconsf.com/sponsors
# todo: find a table that shows sponsorship levels and their cost
numberOfPlatinumSponsors = 1
revenueFromPlatinumSponsors = 50000 # check
numberOfSilverSponsors = 22
revenueFromSilverSponsors = 25000 # check
numberOfBronzeSponsors = 10
revenueFromBronzeSponsors = 10000 # check

revenueFromAttendees = (numberOfRegularAttendees * costOfRegularAttendance) \
    + (numberOfDeluxeAttendees * costOfDeluxeAttendance)
print("revenueFromAttendees: ", revenueFromAttendees)

revenueFromSponsors = (numberOfPlatinumSponsors * revenueFromPlatinumSponsors) \
    + (numberOfSilverSponsors * revenueFromSilverSponsors) \
    + (numberOfBronzeSponsors * revenueFromBronzeSponsors)
print("revenueFromSponsors: ", revenueFromSponsors)

totalRevenue = revenueFromAttendees + revenueFromSponsors
print("total revenue: ", totalRevenue)

percentRevenueFromAttendees = revenueFromAttendees / totalRevenue
print("percentRevenueFromAttendees: ", percentRevenueFromAttendees)

percentRevenueFromSponsors = revenueFromSponsors / totalRevenue
print("percentRevenueFromSponsors: ", percentRevenueFromSponsors)

# Estimate the cost