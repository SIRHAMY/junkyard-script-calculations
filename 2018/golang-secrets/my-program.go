package main
import (
    "fmt"
    "github.com/BurntSushi/toml"
)

func main() {
    pathToSecretFile := "secrets.toml"

    var environmentVariables EnvironmentVariables
    if _, err := toml.DecodeFile(
        pathToSecretFile,
        &environmentVariables); err != nil {
		panic(err)
	}

    fmt.Printf("Secret key: %#v", environmentVariables.Secret_api_key)
}

// BurntSushi/toml requires a struct to
// model the form of the secrets in
// order to read properly
type EnvironmentVariables struct {
    // The struct and properties must both be public
    // in order for BurntSushi/toml to reference them
    Secret_api_key string
}